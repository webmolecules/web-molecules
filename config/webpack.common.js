const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const paths = require('./paths');
const path = require('path');

module.exports = {
	

	//Where webpack looks to start building the bundle
	//entry: path.join(__dirname, "src", "index.js"),
	//entry: [paths.src + '/index.js'],
	entry: {
		//main: path.resolve(__dirname + '/../src/index.js'),			
		main: paths.src + '/index.js'
	},

	output: {
		//path: path.resolve( __dirname + "/../dist/"),   	
		path: paths.build,
		//path: path.resolve( __dirname + "/../build/"),   	
		//filename: '[name].bundle.js',
		//publicPath: '/',//?
	},
	
	plugins: [

		// Removes/cleans build folders and unused assets when rebuilding
		new CleanWebpackPlugin(),

		// Copies files from target to destination folder
		new CopyWebpackPlugin({			
			patterns: [
				{
					from: paths.public,
					to: 'assets',
					globOptions: {
					ignore: ['*.DS_Store'],
					},
					noErrorOnMissing: true,
				},
			],
		}),

		// Generates an HTML file from a template
		// Generates deprecation warning: https://github.com/jantimon/html-webpack-plugin/issues/1501
		new HtmlWebpackPlugin({
			title: 'webpack Boilerplate',
			//template: paths.public + '/index.html',	
			template: path.resolve( __dirname + "/../public/index.html"),// template file							
			//favicon: paths.src + '/assets/images/favicon.png',
			favicon: path.resolve( __dirname + "/../src/assets/images/favicon.png"),// template file		
			filename: 'index.html', // output file
		}),	
	],

	module: {	
		rules: [
			// JavaScript: Use Babel to transpile JavaScript files
			//{ test: /\.js$/, use: ['babel-loader'] },
			{
				//test: /\.?js$/,	
				test: /\.js$|jsx/,		
				exclude: /node_modules/,
				use: {
					loader: "babel-loader",
					//include: path.resolve(__dirname, 'src'),
					options: {
						presets: ['@babel/preset-env', '@babel/preset-react'],
						"plugins": [
							["@babel/plugin-transform-runtime",
								{
									"regenerator": true
								}
							]
						],
					},
				}
			},

			// Images: Copy image files to build folder
			//{ test: /\.(?:ico|gif|png|jpg|jpeg)$/i, type: 'asset/resource' },

			// Fonts and SVGs: Inline files
			//{ test: /\.(woff(2)?|eot|ttf|otf|svg|)$/, type: 'asset/inline' },

			

			

		]
	},

	/*
	performance: {
		hints: false,
		maxEntrypointSize: 512000,
		maxAssetSize: 512000
	},
	*/

	resolve: {
		modules: [paths.src, 'node_modules'],
		extensions: ['', '.ts', '.js', '.jsx', '.json','.scss'],
		alias: {
			'@': paths.src,
			assets: paths.public,
		},
	},
}