const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const { merge } = require('webpack-merge');

const paths = require('./paths');
const path = require('path');
const common = require('./webpack.common');

module.exports = merge(common, {
  mode: 'production',
  devtool: false,
  output: {
    path: paths.build,
    //path: path.resolve(__dirname, './build'),
    publicPath: '/',// required for font loading on historyApiFallback
    filename: 'js/[name].[contenthash].bundle.js',
  },

  module: {
    rules: [
      {
        test: /\.(sass|scss|css)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              //importLoaders: 2,
              sourceMap: false,
              //modules: false,
            },
          },
          {
            //Process CSS with PostCSS
            loader: "postcss-loader",
            options: {
              //sourceMap: true,
              postcssOptions: {
                //config: path.resolve(__dirname + '/../postcss.config.js'),	  
                config: paths.src + '/../postcss.config.js'         
              },
            },
          },	
          {
            //compiles Sass to CSS
            loader: "sass-loader",
            options: {
              //sourceMap: true,
              sassOptions: {
                outputStyle: "compressed",
              },
            },
          },
        ],
      },
      {
				test: /\.(png|svg|jpe?g|gif|woff|woff2|eot|ttf|tiff|mp3|mp4|webm)(\?[a-z0-9=.]+)?$/i,
				use: [
					{						
					loader: 'url-loader',
					options: {
						limit: 8192,
						esModule: false,
					},
					},
				],
			}, 
    ],
  },

  plugins: [
    // Extracts CSS into separate files
    new MiniCssExtractPlugin({
      filename: 'styles/[name].[contenthash].css',
      chunkFilename: '[id].css',
    }),
  ],
  
  optimization: {
    minimize: true,
    minimizer: [new CssMinimizerPlugin(), '...'],
    runtimeChunk: {
      name: 'runtime',
    },
  },

  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000,
  },
 
})
