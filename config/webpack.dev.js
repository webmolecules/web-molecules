const { merge } = require('webpack-merge')
const common = require('./webpack.common')
const paths = require('./paths');
const path = require('path');
module.exports = merge(common, {
  // Set the mode to development or production
  mode: 'development',

  // Control how source maps are generated
  devtool: 'inline-source-map',

  // Spin up a server for quick development
  devServer: {
    historyApiFallback: true,
  	open: true,
    compress: true,
    hot: true,
    port: 8080,
  },

  module: {
    rules: [
      		{
				test: /\.(css|scss|sass)$/i,
				use: [
						{
							//creates style nodes from JS strings
							loader: 'style-loader',
							//options: {
								//sourceMap: true,
								//convertToAbsoluteUrls: true
							//}
						},

						{
							//CSS to CommonJS (resolves CSS imports into exported CSS strings)
							loader: 'css-loader',
							options: {
								sourceMap: true,
								//importLoaders: 2
								//url: false,
								//import: false
							},
						},

						{
							//Process CSS with PostCSS
							loader: "postcss-loader",
							options: {
								postcssOptions: {
									//config: path.resolve(__dirname + '/../postcss.config.js'),	  
									config: paths.src + '/../postcss.config.js'         
								},
							},
						},

						{
							//compiles Sass to CSS
							loader: "sass-loader",
							options: {
								sourceMap: true,
								sassOptions: {
									outputStyle: "compressed",
								},
							},
						},
					]
			},

     		{
				test: /\.(png|svg|jpe?g|gif|woff|woff2|eot|ttf|tiff|mp3|mp4|webm)(\?[a-z0-9=.]+)?$/i,
				use: [
					{						
					loader: 'url-loader',
					options: {
						limit: 8192,
						esModule: false,
					},
					},
				],
			},      

			/* {
				test: /\.(png|svg|jpe?g|gif|woff|woff2|eot|ttf|tiff|mp3|mp4|webm)(\?[a-z0-9=.]+)?$/i,
				include : path.join(__dirname, 'src/assets/images'),
				use: [
					{			  
						loader: 'file-loader',						
						//loader: 'file-loader?name=assets/[name].[ext]',
						options: {								
							//name: 'src/assets/images/[name].[ext]',
							//name: "[name].[hash].[ext]",
							esModule: false,
							//outputPath: "assets/images",
						},
					},
				],
			}, */
    	],
  	},
})
