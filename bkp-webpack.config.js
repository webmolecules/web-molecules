const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  devServer: {
    historyApiFallback: true
  },
  entry: path.join(__dirname, "src", "index.js"),
  output: {
    path:path.resolve(__dirname, "dist"),
  },
  module: {
    rules: [
      {
        //test: /\.?js$/,	
		test: /\.js$|jsx/,		
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
		  //include: path.resolve(__dirname, 'src'),
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react'],
			"plugins": [
				["@babel/plugin-transform-runtime",
				  {
					"regenerator": true
				  }
				]
			 ],
          },
        }
      },	  
      {
        test: /\.css|scss|sass$/i,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader',
        ]
      },
      {
        //test: /\.(png|svg|jpe?g|gif|woff|woff2|eot|ttf||tiff||mp4|webm)$/i,
		test: /\.(png|svg|jpe?g|gif|woff|woff2|eot|ttf||tiff||mp4|webm)(\?[a-z0-9=.]+)?$/i,
		//include: /src/,
        use: [
          {			  
            loader: 'file-loader',
			//loader: 'file-loader?name=assets/[name].[ext]',
            options: {
              //name: 'src/assets/images/[name].[ext]',
              //publicPath: '/',
              esModule: false,
            },
          },
        ],
      },     
    ]
  },
  /*
  performance: {
        hints: false,
        maxEntrypointSize: 512000,
        maxAssetSize: 512000
  },
 */
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, "public", "index.html"),
    }),
  ],
  resolve: {
    extensions: ['.ts', '.js', '.jsx', '.scss'],
  },  
}