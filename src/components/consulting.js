import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link, Element, Events, animateScroll, scrollSpy, scroller } from "react-scroll";
import data from "../data/consulting.json";
import submenuData from "../data/submenu.json";
import SectionHeader from './section-header';
import bgImg from '../assets/images/stock-photo-cropped-shot-group-of-businesspeople-using-graph-chart-and-computer-laptop-tablet-discussing-about-1.jpg';

export default class Consulting extends Component {
	constructor (props) {
        super(props);
        this.state = {
            videoURL: '../assets/images/stock-photo-cropped-shot-group-of-businesspeople-using-graph-chart-and-computer-laptop-tablet-discussing-about-1.jpg',
			scrollTo : "scrollLink6",
			title : "IT Consulting",
			subTitle : "Let's share our views and ideas!",
			navBg : bgImg			
        }
    }

	render() {	  
		return (
			<div className="section-container">
				<SectionHeader 
					title={this.state.title} 
					subTitle={this.state.subTitle} 
					links={submenuData.consulting}
					scrollTo={this.state.scrollTo}
					navBg={this.state.navBg} 
				/>           
				<Container className="content">
					<Row className="justify-content-end gx-60">
						 <Col md={6} className="content-right" style={{textAlign: "justify"}}> 
						  <p>
							{data.intro}
						  </p>
						</Col>
					</Row>        
				</Container>
				<div className="video-container">
					<div className="ratio ratio-21x9">
						<div>
							<video autoPlay loop muted playsInline>
								<source src={require('./../assets/images/stock-footage-group-of-asian-business-people-meeting-in-modern-office-conference-room.webm')} type="video/webm" />
								{/*<source src={require('./../assets/images/stock-footage-group-of-asian-business-people-meeting-in-modern-office-conference-room.mp4')} type="video/mp4" />*/}
								Your browser does not support the video tag.
							</video>
							{/*https://www.codegrepper.com/code-examples/javascript/react+video+player*/}
						</div>
					</div>
				</div>
			</div>
		);
	}  
}
