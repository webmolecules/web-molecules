import React, { Component, useState  } from 'react';
import { Container, Row, Col, InputGroup, Button, FormControl, Accordion, Card } from 'react-bootstrap';

import data from "../data/contact.json";
import SectionHeader from './section-header';
import bgImg from '../assets/images/contact.jpg';
import { Link } from "react-router-dom";
import submenuData from "../data/submenu.json";

export default class Contact extends Component {
	constructor(props) {
		super(props);
		this.state = {			
			collapsed: "collapse3",
			toggleIcon: "up",
			scrollTo : "scrollLink7",
			title : "Lets Connect!",
			subTitle : "Ring our bell and say Hello, we will get back to you",	
			navBg : bgImg	
		}
		this.toggleClass = this.toggleClass.bind(this);
	}
	

	
	toggleClass(e) {
		this.setState({
			collapsed: !this.state.collapsed,
			toggleIcon: this.state.collapsed ? "up" : "down"
		});
	}	
	
	render() {    
	
		return (
			<div className="section-container">
				<SectionHeader 
					title={this.state.title} 
					subTitle={this.state.subTitle} 
					links={submenuData.contact}
					scrollTo={this.state.scrollTo}
					navBg={this.state.navBg} 
				/>
				
				<Container className="content">
					 
					<Row className="get-in-touch">  
						<Col>                
							<h4 className="mb-20">Get in Touch</h4>
						</Col> 
					</Row>
					<Row className="get-in-touch">
						<Col lg={4}>                
						   	<Accordion className="mb-30 address" defaultActiveKey="0">
								<Card>
									<Card.Header>
										<Accordion.Toggle as={Button} variant="link" eventKey="0" onClick={this.toggleClass} className={this.state.collapsed  ? "down" : "up" }>
										India
										</Accordion.Toggle>
									</Card.Header>
									<Accordion.Collapse eventKey="0">
										<Card.Body>Hello! I'm the body</Card.Body>
											</Accordion.Collapse>
										</Card>
									<Card>
									<Card.Header>
										<Accordion.Toggle as={Button} variant="link" eventKey="1" onClick={this.toggleClass} className={ this.state.collapsed  ? "down" : "up" }>
											UK
										</Accordion.Toggle>
										</Card.Header>
											<Accordion.Collapse eventKey="1">
										<Card.Body>Hello! I'm another body</Card.Body>
									</Accordion.Collapse>
								</Card>
							</Accordion>
							<h4>Join our Mailing List</h4>
							<InputGroup className="mb-3">
								<FormControl
								  placeholder="john@yahoo.com"
								  aria-label="Recipient's username"
								  aria-describedby="basic-addon2"
								/>
								<Button variant="outline-secondary" id="button-addon2">
								  Join Now!
								</Button>
							</InputGroup>
						
								<p>Follow us on social media</p>
							
						</Col>
						
						<Col lg={8}>                
							<Row>
								<Col xs={12} lg={6}>                
									<h5>Say Hello</h5>
									<a href="tel:+95544854455711">95544854455711</a>
									<p>9:00 AM - 5:00 PM</p>		   
								</Col>								
								<Col xs={12} lg={6}>                
									<h5>Inquiries</h5>
									<Link to="mailto:inquiry@wm.com">inquiry@wm.com</Link>
									<p>Set up a meeting with us.</p>
								</Col>   
								<Col xs={12} lg={6}>          
									<h5>Quoatation</h5>
									<Link to="mailto:quotation@wm.com">quotation@wm.com</Link>
									<p>Get a quotation within a business day.</p>
								</Col>
								<Col xs={12} lg={6}>                
									<h5>Careers</h5>
									<Link to="mailto:careers@wm.com">careers@wm.com</Link>
									<p>Be a part of our innovative team.</p>
								</Col> 
							</Row>
						</Col> 
						
					</Row>
					
					<Row>           
						<Col xs={12}>                
							<h4>Get a Quotation</h4>  
						</Col> 
						<Col xs={12}>                
							<Row> 
								<Col xs={2}>                
									<h5>Your Detail</h5>  
								</Col> 
								<Col xs={10}>                
									test  
								</Col> 
								<Col xs={2}>                
									<h5>Your Project Requirements</h5>  
								</Col> 
								<Col xs={10}>                
									test  
								</Col> 
							</Row>  
						</Col> 
					</Row> 

					<Row>           
						<Col xs={12}>                
							<h4>FAQ</h4>  
						</Col> 
						<Col xs={12}>                
							<p>{data.intro}</p> 
						</Col> 						
					</Row>					
					<Row>           
						<Col md={4}>
							<p>Blog</p>
						</Col>
						<Col md={4}>
							<p>Twitter Feed</p>
						</Col> 
						<Col lg={4}> 
							<p>News Feed</p>
						</Col>						
					</Row>
				</Container>
		   </div>
		);
    }  
}