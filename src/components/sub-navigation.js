import React, { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import data from "../data/web-design.json";

//export default class SubNavigation extends Component { 
const SubNavigation = () => {
  // Toggle active class
  const [isActive, setActive] = useState("false");

  const ToggleClass = () => {
    setActive(!isActive);
  };

  return (
    <div className="subNavigation"> 
      <div className={isActive ? "sub-links" : "collapsed sub-links"}>
        Navs
      </div>
      <Container>        
        <Row xs="auto"className="justify-content-end  g-0">     
          <Col>
            <button type="button" className={isActive ? "navbar-toggler collapsed" : "navbar-toggler"} onClick={ToggleClass}  data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              {/*<span class="sr-only">Toggle navigation</span>*/}
              <span className="icon-bar top-bar"></span>
              <span className="icon-bar middle-bar"></span>
              <span className="icon-bar bottom-bar"></span>
            </button>
          </Col>
        </Row>
      </Container>
    </div>
  );

  
}
export default SubNavigation;