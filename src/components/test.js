import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';

export default class Test extends Component { 
  render() {    
    return (
      <div>
        <Container className="content mt-60 pt-60">
          <Row>
            <Col>               
              <p>
                Test
              </p>			  
            </Col>
          </Row>                 
        </Container>
      </div>
     );
  }  
}