import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Container, Row, Col } from 'react-bootstrap';
import {NavLink} from 'react-router-dom';

export default class Practice extends Component {	
	render() {
		return (
			<div>
				<Container className="content mt-60 pt-60">
				  <Row>
					<Col xs="12">
						<NavLink to='./form'>How to handle multiple input field in react form with a single function?</NavLink>					 			  
					</Col>
					<Col xs="12">  
					<NavLink to='./dropdown'>Multilevel dropdown menu</NavLink>
					</Col>
					<Col xs="12">  
					<NavLink to='./quiz-app'>Quiz app</NavLink>
					</Col>
					<Col xs="12">  
					<NavLink to='./react-redux-counter'>React Redux Counter</NavLink>
					</Col>
				  </Row>                 
				</Container>
			</div>			
		 );
	}  
}

