import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';


import { useSelector, useDispatch } from 'react-redux';
import { incNumber, deNumber } from './actions/index';


	const ReactReduxCounter = () => {

	// const myState = useSelector((state) => state.changeTheNumber);
//const dispatch = useDispatch(); 
		//const counter = useSelector();
		return (
			
			<Container className="content mt-60 pt-60">
				  <Row>
					<Col xs="12">						
						<h1>Increment/Decrement counter</h1>
						<h4>using React and Redux</h4>
						<div>
							<br/>
							<a title="Decrement" ><span>-</span></a>
							<br/>
							<a title="Increment" ><span>+</span></a>
							<br/>
							<input name="quantity" type="text"/>
							</div> 
					</Col>					
				  </Row>                 
				</Container>			
		 
		 
		 
		 
		 
		 
		 
		 
		 );
	 
}
export default ReactReduxCounter;