import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link, Element, Events, animateScroll, scrollSpy, scroller } from "react-scroll";
import data from "../data/web-development.json";
import submenuData from "../data/submenu.json";
import SectionHeader from './section-header';
import navBgImg from '../assets/images/stock-photo-some-sticky-notes-with-internet-concepts-such-as-web-project-php-css-cms-html-or-j-on-an-345302762.jpg';


export default class WebDevelopment extends Component {
  state = {
    scrollTo : "scrollLink4",
    title : "Web Development",
    subTitle : "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
	navBg : navBgImg    
  } 

  render() {
	// Background image
    var bgImg = {
      backgroundImage: 'url(' + require('../assets/images/stock-photo-writing-note-showing-cloud-software-business-photo-showcasing-programs-used-in-storing-accessing-144.png') + ')',    
    }
    return (
      <div style={bgImg} className="section-container">
		<SectionHeader 
			title={this.state.title} 
			subTitle={this.state.subTitle} 
			links={submenuData.webDevelopment}
			scrollTo={this.state.scrollTo}
			navBg={this.state.navBg} 
		/>
		<Container className="content px-30">  
			<Row className="justify-content-end gx-60">
			  <Col md={6}>          
				<p>
				  {data.intro}          
				</p>				
			  </Col>
			</Row>           
		</Container>
      </div>
    );
  }  
}
