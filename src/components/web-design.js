import React, { Component, useState, useEffect } from 'react';
import ReactDOM from "react-dom";
import { Container, Row, Col } from 'react-bootstrap';
import { Link, Element, Events, animateScroll, scrollSpy, scroller } from "react-scroll";
import data from "../data/web-design.json";
import submenuData from "../data/submenu.json";
import parse from 'html-react-parser';
import SectionHeader from './section-header';
import navBgImg from '../assets/images/stock-photo-website-designer-creative-planning-application-developer-development-draft-sketch-drawing-templa.jpg';


export default class WebDesign extends Component { 
  state = {
    scrollTo : "scrollLink3",
    title : "Web Design",
    subTitle : "Dress up your web identity as per current trend and your business culture",
    navBg : navBgImg	
  }
  
  render() {

    //Background image
    var bgImg = {
      backgroundImage: 'url(' + require('../assets/images/web-design.png') + ')', 
    }

    //!this.state.collapsed ? console.log('not collapsed') : console.log('collapsed');
    //if (this.state.collapsed) {((e) => {
        //console.log('is collapsed after click')
 
        //let element = e.target;
        //console.log(element);//undefined  
  
        //let currentScrollPosition = window.pageYOffset;    
        //console.log(currentScrollPosition)  
        //if (!currentScrollPosition) {
          //console.log('scroll')
        //}
       
    //  })();    
    //}
    
  return (
    <div style={bgImg} className="section-container">
    {/*<div style={bgImg} ref="category_scroll" onWheel={ event => {
        if (this.state.collapsed) {
        this.setState({
          collapsed: !this.state.collapsed
        });          
        if (event.nativeEvent.wheelDelta > 0) {
          console.log('scroll up');
        } else {
          console.log('scroll down');
        }          
      }              
    }}>*/}
		<SectionHeader 
			title={this.state.title} 
			subTitle={this.state.subTitle} 
			links={submenuData.webDesign}
			scrollTo={this.state.scrollTo}
			navBg={this.state.navBg} 
		/>
		<Container className="content px-30">  
			<Row className="justify-content-end gx-60">
			  <Col md={6}>          
				<p>
				  {/*parse({data.intro});*/}
				  {data.intro}          
				</p>
				{data.psersonData &&
				  data.psersonData.map(({ name, id }) => (
					<div key={id} className="row">
					  <strong>{name}</strong>
					</div>
				  ))}
			  </Col>
			</Row>           
		</Container>
    </div>
   );
  }
}
