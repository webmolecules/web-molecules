import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link, Element, Events, animateScroll, scrollSpy, scroller } from "react-scroll";
import data from "../data/ecommerce.json";
import submenuData from "../data/submenu.json";
import SectionHeader from './section-header';
import bgImg from '../assets/images/stock-photo-travel-woman-choosing-lanterns-in-hoi-an-vietnam-1477602647.jpg';

export default class Page extends Component {
	state = {
		title : "Lorem ipsum dolor sit amet",
		subTitle : "Lorem Ipsum",
		navBg : bgImg
	}
	render() {
		return (
			<div className="section inner-page">
			  <div className="section-container">
				<SectionHeader 
					title={this.state.title} 
					subTitle={this.state.subTitle} 
					links={submenuData.portfolio}					
					navBg={this.state.navBg} 
					bg={this.state.navBg} 
				/> 
				<Container className="content">
				  <Row className="mt-60 text-center"> 
					<Col>
					  <p>{data.intro}</p>
					</Col>     
				  </Row>        
				</Container>
			  </div>
			</div>
		 );
	}  
}