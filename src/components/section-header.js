import React, { Component, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { Container, Row, Col, Nav } from 'react-bootstrap';
import { Link, DirectLink, Element, Events, animateScroll, scrollSpy, scroller } from "react-scroll";
import ReactDOM from 'react-dom';

export default class SectionHeader extends Component {
	constructor(props) {
		super(props);
		//console.log(this.props);
		this.activeClass = 'active';
		this.collapsedClass = "collapsed";
		this.state = {
			collapsed: false,
			handleScrolling: false
		}
		this.toggleClass = this.toggleClass.bind(this);
		this.handleScroll = this.handleScroll.bind(this);
		this.handleClick = this.handleClick.bind(this);
		this.onSetActiveFn = this.onSetActiveFn.bind(this);
		this.handleSetActive = this.handleSetActive.bind(this);
	}


	removeSubmenuClass() {
		let navTop = document.getElementsByClassName('nav-top')[0];
		navTop.classList.remove('no-bg');
		let secHead = document.querySelector('.section-header');
		secHead.classList.remove('show');
	}

	toggleClass(e) {
		console.log(e.currentTarget);
		console.log(e.currentTarget.classList.contains(this.activeClass));//true|false    
		if (e.currentTarget.classList.contains(this.activeClass)) {
			console.log('has active class')
		}

		//Find dom node
		//const node = ReactDOM.findDOMNode(this);    
		// Get child nodes
		//if (node instanceof HTMLElement) {
		//const child = node.querySelector('.navbar-toggler');
		//console.log(child);
		//}


		//Find dom node
		//const node = ReactDOM.findDOMNode(this);    
		// Get child nodes
		//if (node instanceof HTMLElement) {
		//const child = node.querySelector('.navbar-toggler.active');
		//console.log(child);
		//if (child.classList.contains(this.activeClass)) {
		//console.log('has active class')
		//} 
		//}


		if (location.pathname == '/') {
			Events.scrollEvent.register('end', (to, element) => {
				//console.log('end', arguments);
				this.toggleState(e);
				this.togleNavBg(e);
			});
		}
		else {
			this.toggleState(e);
			this.togleNavBg(e);
		}
		//if (this.state.collapsed) {
		//	console.log('collapsed')
		//	this.setState({handleScrolling: false});
		//}
	}

	toggleState(e) {
		//console.log(e.currentTarget);
		//toggle state
		this.setState({
			collapsed: !this.state.collapsed
		});
	}
	togleNavBg(e) {
		//top navitation toggle 'no-bg' class
		let navTop = document.getElementsByClassName('nav-top')[0];
		navTop.classList.toggle('no-bg');
	}

	handleClick() {
		//window.removeEventListener('scroll', this.handleScroll, true); 
		this.toggleState();
		this.togleNavBg();
	}

	onSetActiveFn(to) {
		console.log("active", to);
	}

	//onSetInactiveFn(to) {
	//console.log("inactive", to);
	//}
	// Function to handle the activation of a link.
	handleSetActive(to) {
		console.log(to);
	}


	componentDidMount() {
		this.removeSubmenuClass();
		//add scroll event to an element

		window.addEventListener('scroll', this.handleScroll, true);

		//add wheel event to an element   
		//ReactDOM.findDOMNode(this.refs.category_scroll).addEventListener('wheel', this.handleScroll);
		//add wheel event to an element  
		//window.addEventListener('wheel', this.handleScroll);
		//window.addEventListener('wheel', event => {
		//if (this.state.collapsed) {
		//console.log("is collapsed after wheel");
		//}              
		//});
		//window.addEventListener('click', event => {         
		//if (this.state.collapsed) {
		//console.log("is collapsed after click");
		//}              
		//});

		//scrollSpy.update();
	}

	componentWillUnmount() {
		//remove scroll event from an element
		window.removeEventListener('scroll', this.handleScroll);
		//remove wheel event from an element
		//ReactDOM.findDOMNode(this.refs.category_scroll).removeEventListener('wheel', this.handleScroll);
		//remove wheel event from an element
		//window.removeEventListener('wheel', this.handleScroll);  

		//Events.scrollEvent.remove('begin');   
		Events.scrollEvent.remove('end');
		//scrollSpy.update();		  
	}

	handleScroll(e) {
		//console.log(e.currentTarget);

		let navToggler = document.querySelector('.section-header .navbar-toggler.active');
		//console.log(navToggler.classList.contains(this.collapsedClass));
		//if (e.currentTarget.classList.contains(this.activeClass)) {
		//  console.log('has active class')
		//} 

		//console.log('scrolled');
		if (this.state.collapsed) {
			this.setState({ handleScrolling: true });


		}

		if (this.state.collapsed && this.state.handleScrolling) {
			//this.setState({collapsed: true});
			//this.togleNavBg(e);		 
		}
		else {
			//this.setState({collapsed: false});
		}

		if (this.state.collapsed && this.state.handleScrolling === true) {
			//  this.setState({collapsed: true})	

		} else {
			//this.setState({collapsed: false});
		}

		//(this.state.collapsed && this.state.handleScrolling) ? 
		//console.log('collapsed') : 
		//console.log('not collapsed');


		if (this.state.collapsed) {
			this.toggleState(e);
			this.togleNavBg(e);
			//setTimeout(() => {				
			//this.toggleState(e);
			//}, 1000);		 
		}
	}
	render() {
		// Background image	
		var subMenuBg = {
			//backgroundImage: 'url(' + require(`${bgPath}`) + ')',
			backgroundImage: 'url(' + this.props.navBg + ')',
		}
		var bgImg = {
			backgroundImage: 'url(' + this.props.bg + ')',
		}
		return (
			<div style={bgImg} className={this.state.collapsed ? "section-header show" : "section-header"}>
				<Container>
					<Row xs="auto">
						<Col className="pe-0">
							<h2 className="section__title">
								{this.props.title}
							</h2>
							<p className="section__subtitle">{this.props.subTitle}</p>
						</Col>
						<Col className="ps-0">
							{location.pathname == '/' ?
								<Link
									activeClass={this.state.collapsed ? "active" : ""}
									className={this.state.collapsed ? "navbar-toggler collapsed" : "navbar-toggler"}
									//onClick={this.toggleClass}
									onClick={this.toggleClass}
									to={this.props.scrollTo}
									//onSetActive={this.state.collapsed ? () => {this.handleClick} : ""	}
									//onSetActive={this.onSetActiveFn}
									spy={true}
									smooth={true}
									duration={500}
									offset={0}
								>
									<span className="hamburger-menu">
										<span className="icon-bar top-bar"></span>
										<span className="icon-bar middle-bar"></span>
										<span className="icon-bar bottom-bar"></span>
									</span>
								</Link>
								:
								<a
									className={this.state.collapsed ? "navbar-toggler active collapsed" : "navbar-toggler active"}
									onClick={this.toggleClass}
								>
									<span className="hamburger-menu">
										<span className="icon-bar top-bar"></span>
										<span className="icon-bar middle-bar"></span>
										<span className="icon-bar bottom-bar"></span>
									</span>
								</a>
							}
							{/*
										<Link activeClass="active" 
										className={this.state.collapsed ? "navbar-toggler collapsed" : "navbar-toggler"}
										onClick={this.toggleClass}

										to={this.props.scrollTo}
										spy={true}
										smooth={true}
										duration={400}
										offset={0}
									> {this.props.title}
										<span className="arrow">
											<span></span><span></span>
										</span>
									</Link>
									*/}
							{/*<Link  
                  activeClass="active"                      
                  className={ this.state.collapsed  ? "navbar-toggler collapsed" : "navbar-toggler" }
                  onClick={this.toggleClass}                           
                  to={this.props.scrollTo} 
                  spy={true}
                  smooth={true}
                  duration={500}
                  offset={-65}       
                >          
                  <span className="icon-bar top-bar"></span>
                  <span className="icon-bar middle-bar"></span>
                  <span className="icon-bar bottom-bar"></span>
                </Link>*/}

						</Col>

					</Row>
				</Container>
				<div style={subMenuBg} className={this.state.collapsed ? "show submenu" : "submenu"}>
					<Container>
						<Row className="justify-content-end  text-end" xs="auto">
							<Col>
								<ul className="nav justify-content-end flex-column">
									{this.props.links && this.props.links.map(({ name, to, id }) => {
										return (
											<li key={id} className="nav-item">
												<Nav.Link as={NavLink} to={to} onClick={this.handleClick} >
													{name} 
												</Nav.Link>
											</li>
										)
									})}
								</ul>
							</Col>
						</Row>
					</Container>
				</div>
			</div>
		);
	}
}