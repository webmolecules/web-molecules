import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link, Element, Events, animateScroll, scrollSpy, scroller } from "react-scroll";
import data from "../data/about.json";
import submenuData from "../data/submenu.json";
import SectionHeader from './section-header';
import Clients from './clients';
import Highlights from './highlights';
import bgImg from '../assets/images/stock-photo-a-young-indian-asian-woman-stands-up-in-front-of-her-diverse-team-and-is-leading-a-meeting-1132.jpg';

export default class About extends Component {	
	state = {
			scrollTo : "scrollLink2",
			title : "About us",
			subTitle : "WebMolecules is a platform of building and enhancing your web identity",
			navBg : bgImg
	}		

	render() {
		return (
			<div className="section-container">
				<SectionHeader 
					title={this.state.title} 
					subTitle={this.state.subTitle} 
					links={submenuData.about}
					scrollTo={this.state.scrollTo}
					navBg={this.state.navBg} 
				/>
				<Container className="content px-30"> 
					<Row className="gx-60">
						<Highlights />
						<Col md={6}> 
							<p>{data.intro}</p>
						</Col>						
					</Row>  				
					<Clients />
					{/*
					{data.psersonData && data.psersonData.map(({ name, id }) => (
								<div key={id} className="row">
								  <strong>{name}</strong>
								</div>
							))}
					*/}
				</Container>
			</div>
		);
	}  
}