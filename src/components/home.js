import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom'; 

import Navigation from './navigation';
import IntroContainer from './intro-container';
import Portfolio from './portfolio';
import About from './about';
import WebDesign from './web-design';
import WebDevelopment from './web-development';
import DigitalMarketing from './digital-marketing';
import Consulting from './consulting';
import Contact from './contact';
import Ecommerce from './ecommerce';
import Test from './test';
import Notfound from './notfound';


export default class Home extends Component {
   render(){
    
    const components = [
      { name: `IntroContainer`, path: `/` },
      { name: `Portfolio`, path: `/portfolio` },
      { name: `About`, path: `/about` },
      { name: `WebDesign`, path: `/web-design` },
      { name: `WebDevelopment`, path: `/web-development` },
      { name: `DigitalMarketing`, path: `/digital-marketing` },
      { name: `Consulting`, path: `/consulting` },
      { name: `Contact`, path: `/contact` },
	  { name: `Ecommerce`, path: `/ecommerce` },
	  { name: `Test`, path: `/test` },	 	 
	  { name: `Notfound`, path: `/*` }
    ];
    
    const COMPONENT_MAP = {
      IntroContainer: IntroContainer,
      Portfolio: Portfolio,
      About: About,
      WebDesign: WebDesign,
      WebDevelopment: WebDevelopment,      
      DigitalMarketing: DigitalMarketing,
      Consulting: Consulting,
      Contact: Contact,
	  Ecommerce: Ecommerce,
	  Test: Test,	 
	  Notfound: Notfound
    };
	
    return(
      <>
      <BrowserRouter>
	    <Navigation/>
        <Switch>
          {components.map((item, index) =>
            item.path == `/` ? (<Route key={index} exact path={item.path} component={COMPONENT_MAP[item.name]}/>)
            : item.path == `/*` ? (<Route key={index} path={item.path} component={COMPONENT_MAP[item.name]}/>)
            : (<Route key={index} path={item.path} component={COMPONENT_MAP[item.name]} />)
          )}               
        </Switch>  
      </BrowserRouter>
      </>
    );  
   } 
}




