import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Container, Navbar, Nav, NavDropdown } from 'react-bootstrap';

export default class Navigation extends Component {
    componentDidMount() {
     // let { clientHeight, clientWidth } = this.refs.navbarRef;  
      //console.log(clientHeight, clientWidth);
     // React.isValidElement(<Navigation />) ? (console.log("nav is present")) : console.log("nav absent");
     
    }
	
    render() {
		
		const navLinks = [
			{ linkName: `Index`, linkPath: `/` },
			{ linkName: `Home`, linkPath: `/home` },
			{ linkName: `Creations`, linkPath: `/portfolio` },
			{ linkName: `About`, linkPath: `/about` },
			{ linkName: `Design`, linkPath: `/web-design` },
			{ linkName: `Development`, linkPath: `/web-development` },
			{ linkName: `Digital`, linkPath: `/digital-marketing` },
			{ linkName: `Consulting`, linkPath: `/consulting` },
			{ linkName: `Contact`, linkPath: `/contact` } 
		];
     
      return (           
		<Navbar bg="light" expand="lg" >	  
			{/*<Navbar bg="light" expand="lg" ref="navbarRef">*/}
		
          <Container>
            <Navbar.Brand>
              <Nav.Link as={NavLink} to="/">Brand Name</Nav.Link>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
              <Nav>
                {navLinks.map((item, index) => 
                  item.linkPath == `/` ? (
                    <Nav.Link as={NavLink} exact to={item.linkPath} key={index}>
                      {item.linkName}
                    </Nav.Link> 
                  )
                  : item.linkPath == `/*` ? (
                    <Nav.Link as={NavLink} to={item.linkPath} key={index}>
                      {item.linkName}
                    </Nav.Link>
                  ) 
                  : (
                    <Nav.Link as={NavLink} to={item.linkPath} key={index}>
                      {item.linkName}
                    </Nav.Link>
                  )            
                )}          
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      );
    }
}
/*
https://stackoverflow.com/questions/43441856/how-to-scroll-to-an-element
https://stackblitz.com/edit/react-ls1dwp
https://stackblitz.com/edit/react-ls1dwp
*/