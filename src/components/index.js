import React, { Component } from "react";
import ReactDOM from "react-dom";
import {Link, DirectLink, Element, Events, animateScroll, scrollSpy, scroller} from "react-scroll";

import Portfolio from './portfolio';
import About from './about';
import WebDesign from './web-design';
import WebDevelopment from './web-development';
import DigitalMarketing from './digital-marketing';
import Consulting from './consulting';
import Contact from './contact';

let components = {
  Portfolio: Portfolio,
  About: About,
  WebDesign: WebDesign,
  WebDevelopment: WebDevelopment,
  DigitalMarketing: DigitalMarketing,
  Consulting: Consulting,
  Contact: Contact
};

class DynamicComponent extends Component {
  render() {	
    const TagName = components[this.props.tag || 'foo'];
    return <TagName/>
  }
}

export default class Index extends Component {
  //componentDidMount() { 
    //let ele = ReactDOM.findDOMNode(this.refs.introContainer).children    
  //}
  
  render() {
	
    const scrollToElement = [
      {component: `Portfolio`,  elementName: `scrollLink1`},
      {component: `About`, elementName: `scrollLink2` },
      {component: `WebDesign`, elementName: `scrollLink3`},
      {component: `WebDevelopment`, elementName: `scrollLink4`},
      {component: `DigitalMarketing`, elementName: `scrollLink5` },
      {component: `Consulting`, elementName: `scrollLink6` },    
      {component: `Contact`, elementName: `scrollLink7` }
    ];
    
    return (
      <>
		{scrollToElement.map((item, i) =>
			<Element key={i} className={'section section-' + item.component} name={item.elementName} >
				<DynamicComponent tag={item.component} />
			</Element> 
		)}
      </>
    );
  }
}