import React, { Component } from 'react';
import { Container, Row, Col, Nav, Tabs, Tab, TabContent, TabContainer, TabPane } from 'react-bootstrap';
import { NavLink } from 'react-router-dom'; 
import { Link, Element, Events, animateScroll, scrollSpy, scroller } from "react-scroll";
import data from "../data/digital-marketing.json";
import submenuData from "../data/submenu.json";
import SectionHeader from './section-header';
import bgImg from '../assets/images/stock-photo-group-adult-hipsters-friends-sitting-sofa-using-modern-gadgets-business-startup-friendship-team.jpg';
import img1 from '../assets/images/billboard.png';
import img2 from '../assets/images/seo.png';
import img3 from '../assets/images/pay-per-click.png';
import img4 from '../assets/images/social-media.png';
import img5 from '../assets/images/email-marketing.png';
import img6 from '../assets/images/browser.png';
import img7 from '../assets/images/editing.png';
import img8 from '../assets/images/sales-optimisation.png';
import img9 from '../assets/images/digital-advertising.png';
import img10 from '../assets/images/branding.png';

export default class DigitalMarketing extends Component {
	state = {
		scrollTo : "scrollLink5",
		title : "Digital Marketing",
		subTitle : "We create your unique web identity",
		navBg : bgImg,
		tabLinks : [
				{
				  "eventKey": "tab1",
				  "img" : img2,
				  "alt" : "Lorem ipsum"
				},
				{
				  "eventKey": "tab2",
				  "img" : img10,
				  "alt" : "Lorem ipsum"
				},
				{
				  "eventKey": "tab3",
				  "img" : img7,
				  "alt" : "Lorem ipsum"
				},
				{
				  "eventKey": "tab4",
				  "img" : img3,
				  "alt" : "Lorem ipsum"
				},
				{
				  "eventKey": "tab5",
				  "img" : img4,
				  "alt" : "Lorem ipsum"
				},
				{
				  "eventKey": "tab6",
				  "img" : img8,
				  "alt" : "Lorem ipsum"
				},
				{
				  "eventKey": "tab7",
				  "img" : img6,
				  "alt" : "Lorem ipsum"
				},
				{
				  "eventKey": "tab8",
				  "img" : img1,
				  "alt" : "Lorem ipsum"
				},
				{
				  "eventKey": "tab9",
				  "img" : img5,
				  "alt" : "Lorem ipsum"
				},
				{
				  "eventKey": "tab10",
				  "img" : img9,
				  "alt" : "Lorem ipsum"
				},
				{
				  "eventKey": "tab11",
				  "img" : img9,
				  "alt" : "Lorem ipsum"
				},
				{
				  "eventKey": "tab12",
				  "img" : img9,
				  "alt" : "Lorem ipsum"
				}
			  ],
		tabPane : [
				{
				  "eventKey": "tab1",	
				  "title": "SEO",
				  "intro" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
				  "to" : "/"
				},
				{
				   "eventKey": "tab2",
				   "title": "Branding",
				   "intro" : "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
				   "to" : "/"
				},
				{
				   "eventKey": "tab3",
				   "title": "Editing",
				   "intro" : "Duis aute irure dolor in reprehenderit. Lorem ipsum dolor sit amet consectetur adipiscing elit.",
				   "to" : "/"
				},
				{
				   "eventKey": "tab4",
				   "title": "PPT/PPC",
				   "intro" : "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
				   "to" : "/"
				},
				{
				   "eventKey": "tab5",
				   "title": "Social Media",
				   "intro" : "Duis aute irure dolor in reprehenderit. Lorem ipsum dolor sit amet consectetur adipiscing elit.",
				   "to" : "/"
				},
				{
				   "eventKey": "tab6",
				   "title": "Sales Optimization",
				   "intro" : "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
				   "to" : "/"
				},
				{
				   "eventKey": "tab7",
				   "title": "Conversion Rate Optimization",
				   "intro" : "Duis aute irure dolor in reprehenderit. Lorem ipsum dolor sit amet consectetur adipiscing elit.",
				   "to" : "/"
				},
				{
				   "eventKey": "tab8",
				   "title": "Digital Advertising",
				   "intro" : "Duis aute irure dolor in reprehenderit. Lorem ipsum dolor sit amet consectetur adipiscing elit.",
				   "to" : "/"
				},
				{
				   "eventKey": "tab9",
				   "title": "Email Marketing",
				   "intro" : "Duis aute irure dolor in reprehenderit. Lorem ipsum dolor sit amet consectetur adipiscing elit.",
				   "to" : "/"
				},
				{
				   "eventKey": "tab10",
				   "title": "Email Marketing",
				   "intro" : "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
				   "to" : "/"
				},
				{
				   "eventKey": "tab11",
				   "title": "Email Marketing",
				   "intro" : "Duis aute irure dolor in reprehenderit. Lorem ipsum dolor sit amet consectetur adipiscing elit.",
				   "to" : "/"
				},
				{
				   "eventKey": "tab12",
				   "title": "Email Marketing",
				   "intro" : "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
				   "to" : "/"
				}
			  ]
	} 

	render() {		
		return (
			<div className="section-container">
				<SectionHeader 
					title={this.state.title} 
					subTitle={this.state.subTitle} 
					links={submenuData.digitalMarketing}
					scrollTo={this.state.scrollTo}
					navBg={this.state.navBg} 
				/>
				<Container className="content px-30">
					<Row className="gx-0">
						<Col>
							<Tab.Container id="left-tabs-example" defaultActiveKey="tab1">
								<Row className="tab-contents justify-content-center text-center">
									<Col xs={12} lg={6}>
										<Tab.Content>
											{this.state.tabPane.map((item, i) => {
												return (
													<Tab.Pane eventKey={item.eventKey} key={i}>
														<h4>{item.title}</h4>
														<p>{item.intro}</p> <NavLink to={item.to}>read more</NavLink>
													</Tab.Pane>
												)}
											)}						
										</Tab.Content>
									</Col>
								</Row>
								<Row className="g-0 tab-links">	
									<Col xs={12}>
										<Nav variant="pills">
											{this.state.tabLinks.map((item, index) => {
												return (
													<Nav.Link eventKey={item.eventKey} key={index}>
														<span>
															<img src={item.img} alt={item.alt} />
														</span>
													</Nav.Link>	
												)}
											)}								
										</Nav>
									</Col>
							  </Row>
							</Tab.Container>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}  
}