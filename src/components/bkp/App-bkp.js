import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom'; 


import Navigation from './navigation';
import Home from './home';
import Design from './design';
import Develop from './develop';
import About from './about';
import Contact from './contact';
import Notfound from './notfound';

class App extends Component {
   render(){
    
    const menuItems = [
      { name: `Home`, path: `/` },
      { name: `Design`, path: `/design` },
      { name: `Develop`, path: `/develop` },
      { name: `About`, path: `/about` },
      { name: `Contact`, path: `/contact` },
      { name: `Notfound`, path: `/*` }
    ];
    
    const COMPONENT_MAP = {
      Home: Home,
      Design: Design,
      Develop: Develop,
      About: About,
      Contact: Contact,
      Notfound: Notfound
    };    
    
    return(    
      <BrowserRouter> 
        <Navigation/>     
        <Switch>
          {menuItems.map(item => {
              var componentName = COMPONENT_MAP[item.name];
              var pathName = item.path;
              item.name == `Home` ? (
              <Route>
                <componentName exact pathName/>
              </Route>
              )
              : item.name == `Notfound` ? (
                <Route>
                  <componentName pathName/>
                </Route>
              )
              : (
                <Route>
                  <componentName pathName/>
                </Route>
              )
            }
          )}
          {/*
          <Route exact path="/" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/contact" component={Contact} />
          <Route path="*" component={Notfound} />
          */}                  
        </Switch>  
      </BrowserRouter>
    );
   } 
}
export default App;




