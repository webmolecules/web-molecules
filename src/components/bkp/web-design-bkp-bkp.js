import React, { Component, useState, useEffect } from 'react';
import ReactDOM from "react-dom";

import { Container, Row, Col } from 'react-bootstrap';
import { Link, DirectLink, Element, Events, animateScroll, scrollSpy, scroller } from "react-scroll";
import data from "../data/web-design.json";
import parse from 'html-react-parser';

//const WebDesign = () => {
  
export default class WebDesign extends Component {  
  constructor(props) {    
    super(props);
    this.myClass = 'collapsed';
    this.state = {
      collapsed: false,
      vPos : 0
      
    }
    this.toggleClass = this.toggleClass.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    //this.listenScrollEvent = this.listenScrollEvent.bind(this);

    this.myRef = React.createRef();

  }
 

  toggleClass() {
    this.setState({
      collapsed: !this.state.collapsed
    })
  }

  if ({isActive}) {
    console.log('1')
  }
  

  onScroll() {
   // let element = e.target
    console.log('123')
  //  if (element.scrollHeight - element.scrollTop === element.clientHeight) {
  
   // }
  }

  onSetActiveFn(to) {
   // console.log("active", to);      
  }
  
  onSetInactiveFn(to) {
   // console.log("inactive", to);
  }  

  componentDidMount() {    
     window.addEventListener('scroll', this.handleScroll);
    //ReactDOM.findDOMNode(this.refs.category_scroll).addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
      window.removeEventListener('scroll', this.handleScroll);
    //  ReactDOM.findDOMNode(this.refs.category_scroll).removeEventListener('scroll', this.handleScroll);    
  }

  handleScroll(event) {
    //console.log('the scroll things', event)
    event.preventDefault();
    event.stopPropagation();
    let ele = ReactDOM.findDOMNode(this.refs.category_scroll);
    
   //  this.toggleClass;
    
    
    //if (ele.classList.contains(this.myClass)) {       
    //   console.log('collapsed');
    //}
    
    if (event.target === event.currentTarget) {
      console.log('ele');
    }

    
  }

  listenScrollEvent(event) {
    console.log('firee'); 
    let ele = ReactDOM.findDOMNode(this.refs.category_scroll);
    console.log(ele)

    //this.setState({
    //    vPos: event.target.body.scrollTop
    //});
  }
  
  render() {
    
   
    // Background image
    var bgImg = {
      backgroundImage: 'url(' + require('../assets/images/bg-design.png') + ')',    
    }

    let scrollingfn = (e) => { return  console.log('test') }
    
    
    
    let scrollToMyRef = () => console.log('active');

   
    let collState = this.state.collapsed;
    if (collState) {
      
      ((e) => {
        //console.log('collapsed')
 
        //let element = e.target;
        //console.log(element);//undefined  
  
       let scrollEvnt = this.handleScroll.bind(this);
        let currentScrollPosition = window.pageYOffset;    
        //console.log(currentScrollPosition)  
        if (!currentScrollPosition) {
          console.log('123')
        }    
       
      })();   
     
    }
    //!this.state.collapsed ? ( console.log('not active') ) : ( scrollToMyRef ); 

   
  
    // Toggle active class
    //const [isActive, setActive] = useState("false");

 

    //const ToggleClass = () => {
    //  setActive(!isActive);
    //};

   
    
     {/*
    let onSetInactiveFn = () => {  
      !isActive ? ( ToggleClass() ) : ( null );   
    }
    */} 

    //!isActive ? ( 
    //  window.addEventListener('scroll', handleScroll)     
    //console.log('test')
    //) : ( null );   


  
    //let handleScroll = () => {
    //  console.log("useEffect 111");
    //}

    {/*
    useEffect(() => {  
      window.addEventListener('scroll', handleScroll());
    })
    
    let handleScroll = (e) => {
     const bottom = e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight;
     if (bottom) {  return console.log('bottom') }
     
    }
*/}
  return (
    <div style={bgImg} onWheel={ event => {
        if (this.state.collapsed) {
          this.setState({
            collapsed: !this.state.collapsed
          });
          {/*                
          if (event.nativeEvent.wheelDelta > 0) {
            console.log('scroll up');
          } else {
            console.log('scroll down');
          }
          */}
        }              
      }}>
    <Container className="intro-header">   
      <Row xs="auto" className="justify-content-end  g-0">
        <Col> 
          <h2 className="intro__title">              
            {data.title}
          </h2>
          <p className="intro__subtitle">{data.subtitle}</p>
        </Col>   
        <Col>  
            <Link  
              activeClass="active"
              onSetActive={this.onSetActiveFn}
              onSetInactive={this.onSetInactiveFn}          
              className={ this.state.collapsed  ? "navbar-toggler collapsed" : "navbar-toggler" }
              onClick={this.toggleClass}
              ref="category_scroll"                 
              to="scrollLink3"  
              spy={true}
              smooth={true}
              duration={500}
              offset={-60}       
            >          
              <span className="icon-bar top-bar"></span>
              <span className="icon-bar middle-bar"></span>
              <span className="icon-bar bottom-bar"></span>
            </Link>
          </Col>     
      </Row>  
    </Container>   
    <div className={ this.state.collapsed  ? "submenu-shown submenu" : "submenu"}>
      <Container>       
        <Row className="justify-content-end  text-end" xs="auto">
          <Col>
            <ul className="nav flex-column">
              <li className="nav-item">
                <a className="nav-link" href="#">eCommerce Websites</a>
              </li>            
              <li className="nav-item">
                <a className="nav-link" href="#">Corporate Websites</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">Web and Mobile Applications</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">Blogs and Portals</a>
              </li>            
              <li className="nav-item">
                <a className="nav-link" href="#">Logos and Brochures</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">Let's discuss your requirements</a>
              </li>
            </ul>
          </Col>
        </Row>
      </Container>
    </div>  
    <Container className="intro-content">  
      <Row>
        <Col md={6}>          
          <p>
            {/*parse({data.intro});*/}
            {data.intro}          
          </p>
          {data.psersonData &&
            data.psersonData.map(({ name, id }) => (
              <div key={id} className="row">
                <strong>{name}</strong>
              </div>
            ))}
        </Col>
      </Row>           
    </Container> 
    
    {/*  
    <div >
      <Container>
        <Row xs="auto"className="justify-content-end  g-0">      
          <Col>
           
            <div className={ this.state.collapsed  ? "navbar-toggler" : "navbar-toggler collapsed"} >
              <button type="button" className="dropdown-toggle btn" onClick={this.toggleClass}  data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar top-bar"></span>
                <span className="icon-bar middle-bar"></span>
                <span className="icon-bar bottom-bar"></span>
              </button>
            </div>
          
          

          
          </Col>               
      </Row>
      <Row>
      <Col> 
            Nav
          </Col>
        </Row>
      </Container>
    </div> 
    */}
    
    
    </div>
   );
  }
}
