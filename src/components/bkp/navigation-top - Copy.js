import React, { Component } from "react";
import { Container, Navbar, Nav } from 'react-bootstrap';
import { Link, Element, Events, animateScroll, scrollSpy, scroller } from "react-scroll";
import { withRouter, NavLink } from "react-router-dom";
import data from "../data/navigation-top.json";

class NavTop extends Component {
	constructor(props) {
		super(props);    
		this.scrollToPage = this.scrollToPage.bind(this);	
		this.removeScrollEvent = this.removeScrollEvent.bind(this);	
	}	

	async scrollToPage(target) {
		//console.log(target);
		//const currentURL = window.location.href 
		//console.log(currentURL);
		//const pathname = window.location.pathname
		//console.log(pathname);
		// location.pathname !=='/' ? console.log('111') : console.log('222');
		
		if (location.pathname !=='/') {
			await this.props.history.push('/');	
			scroller.scrollTo(target, {
				smooth: true, 				
				duration: 500,
				//offset: -65,
				offset: 0,
				//spy: false,
				delay: 0
				//onSetActive: this.onSetActiveFn(target),
				//onSetInactive: this.onSetInactiveFn(target)
			});
			//remove submenu-shown class from page div    
			let pageTheme = document.getElementsByClassName('page')[0];
			pageTheme.classList.remove('submenu-shown');     
		}
		
		Events.scrollEvent.remove("end");
			
	}
	removeScrollEvent() {
		Events.scrollEvent.remove("end");
	}

	//onSetActiveFn(to) {		
      //console.log("active", to);
    //}

    //onSetInactiveFn(to) {
      //console.log("inactive", to);
    //}
	
	render() {	
		
		return (
			<>    
				<Navbar expand="lg" className="nav-top fixed-top" >
					<Container className="px-30">
						<Navbar.Brand>
							<Nav>
							{ location.pathname =='/' ?
							 
							 <Link activeClass="active" 
							  to={data.links[0].to}
							  spy={true} smooth={true} duration={500} offset={-150} onClick={() => {this.removeScrollEvent()}}>
									<span className="logo-container">
										<img className="logo-dark"  src={require('../assets/images/logo-dark.png')} alt="Web Molecules"/>
										<img className="logo-light"  src={require('../assets/images/logo-light.png')} alt="Web Molecules"/>
									</span>
									<span className="logo-text">
										<span>Web</span>
										<span>Molecules</span>
									</span>
								</Link>
								
							:
								
								<NavLink to="./">
									<span className="logo-container">
										<img className="logo-dark"  src={require('../assets/images/logo-dark.png')} alt="Web Molecules"/>
										<img className="logo-light"  src={require('../assets/images/logo-light.png')} alt="Web Molecules"/>
									</span>
									<span className="logo-text">
										<span>Web</span>
										<span>Molecules</span>
									</span>
								</NavLink>
								
							}
							 
							</Nav>
						</Navbar.Brand>
						<Navbar.Toggle aria-controls="basic-navbar-nav"/>              
						<Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
						<ul className="navbar-nav">	
							
						{/*
						{data.links && data.links.map(({id, name, to}) => 
							<Link 
								key={id} 
								activeClass="active" 
								className="nav-link"
								spy={true} 
								to={to}
								smooth={true} 
								duration={500} 
								offset={-65}  
								onClick={() => {this.scrollToPage(to)}}
							>
								{name}
							</Link>	
						)}
						*/}
						{/*
						{
							location.pathname !=='/' ? 
								data.links && data.links.map((item, index) => 
									<div 
										key={item.id} 									
										className="nav-link"
										onClick={() => {this.scrollToPage(item.to)}}
									>
										{item.name}
									</div>	
								)
							: 
								data.links && data.links.map((item, index) => 
									<Link 
										key={item.id} 
										activeClass="active" 
										className="nav-link"
										spy={true} 
										to={item.to}
										smooth={true} 
										duration={500} 
										offset={-65}  
										onClick={() => {this.scrollToPage(item.to)}}
									>
										{item.name}
									</Link>	
								)
						}
						*/}
						{
							data.links && data.links.map((item, index) => 
								<li className="nav-item" key={item.id}>
									<Link 										
										activeClass="active" 
										className="nav-link"
										spy={true} 
										to={item.to}
										smooth={true} 
										duration={500} 
										offset={0}  
										onClick={() => {this.scrollToPage(item.to)}}
									>
										{item.name}
									</Link>	
								</li>
							)
						}
							
						</ul>
						</Navbar.Collapse>
					</Container>
				</Navbar>          
			</>
		);    
	}  
}
export default withRouter(NavTop);