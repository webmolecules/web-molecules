import React, { Component, useState, useEffect, useCallback } from 'react';

import { Container, Row, Col, DropdownButton, Dropdown } from 'react-bootstrap';
import { Link, DirectLink, Element, Events, animateScroll, scrollSpy, scroller } from "react-scroll";
import data from "../data/web-design.json";
import parse from 'html-react-parser';

const WebDesign = () => {

  

  

  // Background image
  var bgImg = {
    backgroundImage: 'url(' + require('../assets/images/bg-design.png') + ')',    
  }

  // Toggle active class
  const [isActive, setActive] = useState("false");

 

  const ToggleClass = () => {
    setActive(!isActive);
  };

  {/*
    let onSetActiveFn = () => {
      console.log("active");
    }
   
  let onSetInactiveFn = () => {  
    !isActive ? ( ToggleClass() ) : ( null );   
  }
  */} 
  !isActive ? ( 
   // window.addEventListener('scroll', handleScroll)     
   console.log('test')
  ) : ( null );   

  let oldScrollY = 0;
  const [direction, setDirection] = useState('up');
  const controlDirection = () => {
    if(window.scrollY > oldScrollY) {
        setDirection('down');
        console.log('down')
    } else {
        setDirection('up');
        console.log('up')
    }
    oldScrollY = window.scrollY;
  }
  
  useEffect(() => {
    window.addEventListener('scroll', controlDirection);  
    return () => {
      window.removeEventListener('scroll', controlDirection);
    };
  },[]);

  if (!isActive) {
    console.log('123');  
  }



  //const [scrollDir, setScrollDir] = useState("scrolling down");

  

  let handleScroll = () => {
    console.log("useEffect 111");
  }
   {/*
  useEffect(() => {  
    window.addEventListener('scroll', handleScroll());
  })

  */}





  return (
    <div>     {/*
    <div className={isActive ? "subNavigation d-none" : "subNavigation"}>
      <Container>
        <Row xs="auto"className="justify-content-end  g-0">      
          <Col>
           
            <div className={isActive ? "navbar-toggler" : "navbar-toggler collapsed"} >
              <button type="button" className="dropdown-toggle btn" onClick={ToggleClass}  data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span className="icon-bar top-bar"></span>
                <span className="icon-bar middle-bar"></span>
                <span className="icon-bar bottom-bar"></span>
              </button>
            </div>
          
          

          
          </Col>               
      </Row>
      <Row>
      <Col> 
            Nav
          </Col>
        </Row>
      </Container>
    </div> */}
    <Container style={bgImg} className="content">   
      <Row xs="auto"className="justify-content-end  g-0">
        <Col> 
          <h2 className="intro__title">              
            {data.title}
          </h2>
          <p className="intro__subtitle">{data.subtitle}</p>
        </Col>
        <Col>  
          <Link 
            activeClass="active"
            className={isActive ? "navbar-toggler" : "navbar-toggler collapsed"}
            to="scrollLink3"
            spy={true}
            smooth={true}
            duration={500}
            offset={-65}
            onClick={ToggleClass}
                 
        
          >          
            <span className="icon-bar top-bar"></span>
            <span className="icon-bar middle-bar"></span>
            <span className="icon-bar bottom-bar"></span>
          </Link>
        </Col>
      </Row>
      <Row>
      <Col md={6}>          
        <p>
          {/*parse({data.intro});*/}
          {data.intro}          
        </p>
        {data.psersonData &&
          data.psersonData.map(({ name, id }) => (
            <div key={id} className="row">
              <strong>{name}</strong>
            </div>
          ))}
      </Col>
      </Row>
      
    </Container> 
    </div> 
  );
  
}

export default WebDesign;