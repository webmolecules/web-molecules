import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom'; 

import NavTop from './navigation-top';
import Navigation from './navigation';
import IntroContainer from './intro-container';
import Portfolio from './portfolio';
import About from './about';
import WebDesign from './web-design';
import WebDevelopment from './web-development';
import DigitalMarketing from './digital-marketing';
import Consulting from './consulting';
import Contact from './contact';
import Notfound from './notfound';

export default class App extends Component {
   render(){
    
    const menuItems = [
      { name: `IntroContainer`, path: `/` },
      { name: `Portfolio`, path: `/portfolio` },
      { name: `About`, path: `/about` },
      { name: `WebDesign`, path: `/web-design` },
      { name: `WebDevelopment`, path: `/web-development` },
      { name: `DigitalMarketing`, path: `/digital-marketing` },
      { name: `Consulting`, path: `/consulting` },
      { name: `Contact`, path: `/contact` },      
      { name: `Notfound`, path: `/*` }
    ];
    
    const COMPONENT_MAP = {
      IntroContainer: IntroContainer,
      Portfolio: Portfolio,
      About: About,
      WebDesign: WebDesign,
      WebDevelopment: WebDevelopment,      
      DigitalMarketing: DigitalMarketing,
      Consulting: Consulting,
      Contact: Contact,
      Notfound: Notfound
    };

    return(
      <>
      <BrowserRouter>
        <Navigation/>
        <NavTop ref="navRef"/>
        <Switch>
          {menuItems.map((item, index) =>
            item.path == `/` ? (<Route key={index} exact path={item.path} component={COMPONENT_MAP[item.name]}/>)
            : item.path == `/*` ? (<Route key={index} path={item.path} component={COMPONENT_MAP[item.name]}/>)
            : (<Route key={index} path={item.path} component={COMPONENT_MAP[item.name]} />)
          )}               
        </Switch>  
      </BrowserRouter>
      </>
    );  
   } 
}




