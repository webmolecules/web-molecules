import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'; 
import { Container, Row, Col, Nav } from 'react-bootstrap';
import { Link, DirectLink, Element, Events, animateScroll, scrollSpy, scroller } from "react-scroll";

export default class SectionHeader extends Component {
	constructor(props) {
		super(props);
		//console.log(this.props);
		this.myClass = 'active';
		this.state = {
		  collapsed: false    
		}
		this.toggleClass = this.toggleClass.bind(this);
		this.handleScroll = this.handleScroll.bind(this);
		this.handleClick = this.handleClick.bind(this);
		this.onSetActiveFn = this.onSetActiveFn.bind(this);		
	}
	
	removeSubmenuClass = () => {
		//remove submenu-shown class from page div 
		let pageTheme = document.getElementsByClassName('page')[0];
		pageTheme.classList.remove('submenu-shown');
	}
	
	toggleClass(e) {
		//console.log("click");
		//console.log(e.currentTarget.classList.contains(this.activeClass));//true|false    
		//if (e.currentTarget.classList.contains(this.activeClass)) {
		//  console.log('has active class')
		//}

		//Find dom node
		//const node = ReactDOM.findDOMNode(this);    
		// Get child nodes
		//if (node instanceof HTMLElement) {
		  //const child = node.querySelector('.navbar-toggler');
		  //console.log(child);
		//}
		
		if (location.pathname =='/') {
			
			Events.scrollEvent.register('end', (to, element) => {
				//console.log('end', arguments);
				this.toggleState();
				this.toggleSubmenuClass();
			});
			
		}
		else {
			this.toggleState();
			this.toggleSubmenuClass();
		}
	}	
	
	toggleState = () => {
		console.log('toggleState');
		//toggle state
		this.setState({
			collapsed: !this.state.collapsed
		});
	}
	
	toggleSubmenuClass = () => {
		//console.log('toggleSubmenuClass');
		//toggle submenu-shown class to page div
		let pageTheme = document.getElementsByClassName('page')[0];
		pageTheme.classList.toggle('submenu-shown');
	}
 
	handleScroll(e) {
		//console.log('scrolled');
		if (this.state.collapsed) {
			console.log('collapsed');
			//setTimeout(() => {
				
			this.toggleState();
			this.toggleSubmenuClass(); 
			//}, 1000);
		 
		}
	}	
	
	handleClick() {				
		window.removeEventListener('scroll', this.handleScroll, true); 
		this.toggleState();
		this.removeSubmenuClass();
	}

	onSetActiveFn(to) {
		//console.log("active", to);
	}

	//onSetInactiveFn(to) {
		//console.log("inactive", to);
	//}
	
	componentDidMount() {  
		this.removeSubmenuClass(); 
		//add scroll event to an element
		
		window.addEventListener('scroll', this.handleScroll, true);
		
		
		//add wheel event to an element   
		//ReactDOM.findDOMNode(this.refs.category_scroll).addEventListener('wheel', this.handleScroll);
		//add wheel event to an element  
		//window.addEventListener('wheel', this.handleScroll);
		//window.addEventListener('wheel', event => {
		  //if (this.state.collapsed) {
			//console.log("is collapsed after wheel");
		  //}              
		//});
		//window.addEventListener('click', event => {         
		  //if (this.state.collapsed) {
			//console.log("is collapsed after click");
		  //}              
		//});

		//scrollSpy.update();
	}

	componentWillUnmount() {
		//remove scroll event from an element
		window.removeEventListener('scroll', this.handleScroll, true); 
		//remove wheel event from an element
		//ReactDOM.findDOMNode(this.refs.category_scroll).removeEventListener('wheel', this.handleScroll);
		//remove wheel event from an element
		//window.removeEventListener('wheel', this.handleScroll);  

		//Events.scrollEvent.remove('begin');   
		Events.scrollEvent.remove('end'); 
		//scrollSpy.update();		  
	}

	render() {
	// Background image	
    var subMenuBg = {		
		//backgroundImage: 'url(' + require(`${bgPath}`) + ')',
		backgroundImage: 'url(' + this.props.navBg + ')',
    }	
	
	var bgImg = {		
		backgroundImage: 'url(' + this.props.bg + ')',
    }
    
	return (
		<div style={bgImg} className="section-header">
			<Container>   
				<Row xs="auto">
					<Col className="pe-0">
						<h2 className="section__title">              
							{this.props.title}
						</h2>
						<p className="section__subtitle">{this.props.subTitle}</p>
					</Col>   
					<Col className="ps-0">  				
						{location.pathname =='/' ? 
							<Link activeClass="active" className={ this.state.collapsed  ? "navbar-toggler collapsed" : "navbar-toggler" }
							  onClick={this.toggleClass}    
							  
							  to={this.props.scrollTo} 
							  spy={true}
							  smooth={true}
							  duration={400}							 
							  offset={0}       
							>          
								<span className="arrow">
									<span></span><span></span>
								</span>
							</Link>
						:
							<a 				                  
							  className={ this.state.collapsed  ? "navbar-toggler active collapsed" : "navbar-toggler active" }
							  onClick={this.toggleClass} 
							>
								<span className="arrow">
									<span></span><span></span>
								</span>
							</a>
						}				
                {/*<Link  
                  activeClass="active"                      
                  className={ this.state.collapsed  ? "navbar-toggler collapsed" : "navbar-toggler" }
                  onClick={this.toggleClass}                           
                  to={this.props.scrollTo} 
                  spy={true}
                  smooth={true}
                  duration={500}
                  offset={-65}       
                >          
                  <span className="icon-bar top-bar"></span>
                  <span className="icon-bar middle-bar"></span>
                  <span className="icon-bar bottom-bar"></span>
                </Link>*/}
              </Col>     
          </Row>  
        </Container>
        <div style={subMenuBg} className={ this.state.collapsed  ? "show submenu" : "submenu"}>
          <Container>       
            <Row className="justify-content-end  text-end" xs="auto">
              <Col>
                <ul className="nav justify-content-end flex-column">  
					{this.props.links && this.props.links.map(({name, to, id}) => {
						return (
							<li key={id} className="nav-item">
								<Nav.Link as={NavLink} to={to} onClick={this.handleClick} >
								  {name}
								</Nav.Link> 
							</li>
						)
					})}
                </ul>
              </Col>
            </Row>
          </Container>
        </div>  
      </div>
    );
  }  
}