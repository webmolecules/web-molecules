import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'; 
import { Container, Row, Col, Nav } from 'react-bootstrap';
import { Link, DirectLink, Element, Events, animateScroll, scrollSpy, scroller } from "react-scroll";

import navBgImg1 from '../assets/images/istockphoto-820421924-1024x1024.png';

export default class IntroHeader extends Component {
	constructor(props) {
		super(props);
		//console.log(this.props);
		this.myClass = 'collapsed';
		this.state = {
		  collapsed: false    
		}
		this.toggleClass = this.toggleClass.bind(this);
		this.handleScroll = this.handleScroll.bind(this);
		this.handleClick = this.handleClick.bind(this);
	}

	toggleClass(e) {
		//console.log("click");
		//console.log(e.currentTarget.classList.contains(this.activeClass));//true|false    
		//if (e.currentTarget.classList.contains(this.activeClass)) {
		//  console.log('has active class')
		//}

		//Find dom node
		//const node = ReactDOM.findDOMNode(this);    
		// Get child nodes
		//if (node instanceof HTMLElement) {
		  //const child = node.querySelector('.navbar-toggler');
		  //console.log(child);
		//}

		Events.scrollEvent.register('end', (to, element) => {
		  //console.log('end', arguments);
		  //toggle state
		  this.setState({
			collapsed: !this.state.collapsed
		  });
		  //toggle submenu-shown class to page-theme div
		  let pageTheme = document.getElementsByClassName('page')[0];
		  pageTheme.classList.toggle('submenu-shown');
		});
	}	
 
	handleScroll(e) {
		//console.log('scrolled');
		if (this.state.collapsed) {
		  //console.log('active');
		  this.setState({
			collapsed: !this.state.collapsed
		  });
		  //toggle submenu-shown class to page-theme div    
		  let pageTheme = document.getElementsByClassName('page')[0];
		  pageTheme.classList.toggle('submenu-shown');     
		}
	}
	
	handleClick() {
		//remove submenu-shown class from page-theme div    
		let pageTheme = document.getElementsByClassName('page')[0];
		pageTheme.classList.remove('submenu-shown');   
	}

	//onFActiveFn(to) {
		//console.log("active", to);
	//}

	//onSetInactiveFn(to) {
		//console.log("inactive", to);
	//}
	componentDidMount() {   
		//add scroll event to an element
		window.addEventListener('scroll', this.handleScroll);
		//add wheel event to an element   
		//ReactDOM.findDOMNode(this.refs.category_scroll).addEventListener('wheel', this.handleScroll);
		//add wheel event to an element  
		//window.addEventListener('wheel', this.handleScroll);
		//window.addEventListener('wheel', event => {
		  //if (this.state.collapsed) {
			//console.log("is collapsed after wheel");
		  //}              
		//});
		//window.addEventListener('click', event => {         
		  //if (this.state.collapsed) {
			//console.log("is collapsed after click");
		  //}              
		//});

		//scrollSpy.update();   
	}

	componentWillUnmount() {
		//remove scroll event from an element
		window.removeEventListener('scroll', this.handleScroll); 
		//remove wheel event from an element
		//ReactDOM.findDOMNode(this.refs.category_scroll).removeEventListener('wheel', this.handleScroll);
		//remove wheel event from an element
		//window.removeEventListener('wheel', this.handleScroll);  

		//Events.scrollEvent.remove('begin');   
		Events.scrollEvent.remove('end'); 
		//scrollSpy.update();   
	}

  render() {
	// Background image
	var bgPath = this.props.navBg
	//var bgFullPath = '../assets/images/' + bgPath
	//const bgPath_map = {
	//		bgPath: bgPath
	//	};
    var bgImg = {
		//backgroundImage: this.props.navBg
		 //backgroundImage: 'url(' + require("../assets/images/" + bgPath) + ')', 
		// backgroundImage: 'url(' + require(`../assets/images/${bgPath}`) + ')', 
		 //backgroundImage: 'url(' + require('../assets/images/istockphoto-820421924-1024x1024.png') + ')', 
		//backgroundImage: navBgImg1
		backgroundImage: 'url(' + this.props.navBg + ')',
    }
	
	
	//console.log( bgImg)
    return (
      <>
        <Container className="intro-header px-30 pt-50 pb-20">   
          <Row xs="auto" className="justify-content-end gx-60">
            <Col className="pe-15">
              <h2 className="intro__title">              
                {this.props.title}
              </h2>
              <p className="intro__subtitle">{this.props.subTitle}</p>
            </Col>   
            <Col className="ps-0">  
				
				<Link  
                  activeClass="active"                      
                  className={ this.state.collapsed  ? "navbar-toggler collapsed" : "navbar-toggler" }
                  onClick={this.toggleClass}                           
                  to={this.props.scrollTo} 
                  spy={true}
                  smooth={true}
                  duration={500}
                  offset={0}       
                >          
                 <span className="arrow">
					<span></span><span></span>
				 </span>
                </Link>
                {/*<Link  
                  activeClass="active"                      
                  className={ this.state.collapsed  ? "navbar-toggler collapsed" : "navbar-toggler" }
                  onClick={this.toggleClass}                           
                  to={this.props.scrollTo} 
                  spy={true}
                  smooth={true}
                  duration={500}
                  offset={-65}       
                >          
                  <span className="icon-bar top-bar"></span>
                  <span className="icon-bar middle-bar"></span>
                  <span className="icon-bar bottom-bar"></span>
                </Link>*/}
              </Col>     
          </Row>  
        </Container>
        <div style={bgImg} className={ this.state.collapsed  ? "show submenu" : "submenu"}>
          <Container>       
            <Row className="justify-content-end  text-end" xs="auto">
              <Col>
                <ul className="nav justify-content-end flex-column">  
					{this.props.links && this.props.links.map(({name, to, id}) => {
						return (
							<li key={id} className="nav-item">
								<Nav.Link as={NavLink} to={to} onClick={this.handleClick} >
								  {name}
								</Nav.Link> 
							</li>
						)
					})}
                </ul>
              </Col>
            </Row>
          </Container>
        </div>  
      </>
    );
  }  
}