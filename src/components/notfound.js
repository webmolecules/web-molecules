import React from 'react'
const Notfound = () => {
    return (
        <div className="intro">
            <div className="container">
            <h1 className="intro__title">Not Found</h1>
            </div>
        </div>
    );
}
export default Notfound;