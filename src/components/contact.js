import React, { Component, useState } from 'react';
import { Container, Row, Col, InputGroup, Button, FormControl, Accordion, Card } from 'react-bootstrap';

import data from "../data/contact.json";
import SectionHeader from './section-header';
import bgImg from '../assets/images/contact.jpg';
import skylineBgImg from '../assets/images/stock-photo-group-adult-hipsters-friends-sitting-sofa-using-modern-gadgets-business-startup-friendship-team.jpg';
import { Link } from "react-router-dom";
import submenuData from "../data/submenu.json";
import * as Icon from 'react-bootstrap-icons';

const Contact = function () {

	const [sectionHeader, setSectionHeader] = useState(
		{
			id: 1,
			collapsed: "collapse3",
			toggleIcon: "up",
			scrollTo: "scrollLink7",
			title: "Lets Connect!",
			subTitle: "Say Hello, we will get back to you",
			navBg: bgImg
		}
	);

	const [activeId, setActiveId] = useState('0');

	function toggleActive(id) {
		if (activeId === id) {
			setActiveId(null);
		} else {
			setActiveId(id);
		}
	}

	//Background image
	var skylineBgImg = {
		backgroundImage: 'url(' + require('../assets/images/stock-vector-statue-of-liberty-and-city-background-from-single-line-background-concept-for-new-york-city-1703236.png') + ')', 
	  }
	return (
		<div style={skylineBgImg} className="section-container">
			<SectionHeader
				title={sectionHeader.title}
				subTitle={sectionHeader.subTitle}
				links={submenuData.contact}
				scrollTo={sectionHeader.scrollTo}
				navBg={sectionHeader.navBg}
				key={sectionHeader.id}
			/>

			<Container className="content">

				<Row className="get-in-touch">


					<Col lg={8}>
						<Row>
							<Col xs={12}>
								<h4>Get in Touch</h4>

							</Col>
							<Col xs={12} lg={6} className='mt-20'>
								<h5>Say Hello</h5>
								<a href="tel:+95544854455711">95544854455711</a>
								<p>9:00 AM - 5:00 PM</p>
							</Col>
							<Col xs={12} lg={6} className='mt-20'>
								<h5>Inquiries</h5>
								<Link to="mailto:inquiry@wm.com">inquiry@wm.com</Link>
								<p>Set up a meeting with us.</p>
							</Col>
							<Col xs={12} lg={6} className='mt-20'>
								<h5>Quoatation</h5>
								<Link to="mailto:quotation@wm.com">quotation@wm.com</Link>
								<p>Get a quotation within a business day.</p>
							</Col>
							<Col xs={12} lg={6} className='mt-20'>
								<h5>Careers</h5>
								<Link to="mailto:careers@wm.com">careers@wm.com</Link>
								<p>Be a part of our innovative team.</p>
							</Col>
							<Col xs={12}>
								<Row>
									<Col xs={12} lg={6} className='mt-40'>
										<h4>Join our Mailing List</h4>
										<InputGroup className="mb-3 mr-60">
											<FormControl
												placeholder="john@yahoo.com"
												aria-label="Recipient's username"
												aria-describedby="basic-addon2"
											/>
											<Button variant="outline-secondary" id="button-addon2">
												Join Now!
											</Button>
										</InputGroup>
									</Col>
								</Row>
							</Col>
						</Row>
					</Col>
					<Col lg={4}>
						<Accordion defaultActiveKey={activeId} className="address">

							<Card className={activeId === "0" ? "active" : ""}>
								<Card.Header>
									<Accordion.Toggle as={Button} variant="link" onClick={() => toggleActive('0')} eventKey="0">
										<Icon.SignpostFill color="#fff" width="1.5rem" height="1.5rem" />
										<span>India</span>
									</Accordion.Toggle>
								</Card.Header>
								<Accordion.Collapse eventKey="0">
									<Card.Body>Lorem ipsum dolor sit amet, <br />
										Consectetur adipiscing elit, <br />
										Sed do eiusmod tempor <br />
										Incididunt ut labore et <br />
										Dolore magna aliqua.</Card.Body>
								</Accordion.Collapse>
							</Card>

							<Card className={activeId === "1" ? "active" : ""}>
								<Card.Header>
									<Accordion.Toggle as={Button} variant="link" onClick={() => toggleActive('1')} eventKey="1">
										<Icon.SignpostFill color="#fff" width="1.5rem" height="1.5rem" /> <span>USA</span>
									</Accordion.Toggle>
								</Card.Header>
								<Accordion.Collapse eventKey="1">
									<Card.Body>Lorem ipsum dolor sit amet, <br />
										Consectetur adipiscing elit, <br />
										Sed do eiusmod tempor <br />
										Incididunt ut labore et <br />
										Dolore magna aliqua.</Card.Body>
								</Accordion.Collapse>
							</Card>
							<Card className={activeId === "2" ? "active" : ""}>
								<Card.Header>
									<Accordion.Toggle as={Button} variant="link" onClick={() => toggleActive('2')} eventKey="2">
										<Icon.SignpostFill color="#fff" width="1.5rem" height="1.5rem" /> <span>UK</span>
									</Accordion.Toggle>
								</Card.Header>
								<Accordion.Collapse eventKey="2">
									<Card.Body>Lorem ipsum dolor sit amet, <br />
										Consectetur adipiscing elit, <br />
										Sed do eiusmod tempor <br />
										Incididunt ut labore et <br />
										Dolore magna aliqua.</Card.Body>
								</Accordion.Collapse>
							</Card>
						</Accordion>
					</Col>
				</Row>
				{/*
				<Row>
					<Col xs={12}>
						<h4>Get a Quotation</h4>
					</Col>
					<Col xs={12}>
						<Row>
							<Col xs={2}>
								<h5>Your Detail</h5>
							</Col>
							<Col xs={10}>
								test
							</Col>
							<Col xs={2}>
								<h5>Your Project Requirements</h5>
							</Col>
							<Col xs={10}>
								test
							</Col>
						</Row>
					</Col>
				</Row>

				<Row>
					<Col xs={12}>
						<h4>FAQ</h4>
					</Col>
					<Col xs={12}>
						<p>{data.intro}</p>
					</Col>
				</Row>
				<Row>
					<Col md={4}>
						<p>Blog</p>
					</Col>
					<Col md={4}>
						<p>Twitter Feed</p>
					</Col>
					<Col lg={4}>
						<p>News Feed</p>
					</Col>
				</Row>
				 */}
			</Container>
		</div>
	);
}
export default Contact;