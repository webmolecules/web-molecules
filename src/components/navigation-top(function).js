import React, { Component, useState } from "react";
import { Container, Navbar, Nav, NavLink } from 'react-bootstrap';
import { Link, Element, Events, animateScroll, scrollSpy, scroller } from "react-scroll";
import { useHistory } from "react-router-dom";
import data from "../data/navigation-top.json";

const NavigationTop = () => {
	//const currentURL = window.location.href 
	//console.log(currentURL);
	//const pathname = window.location.pathname
	//console.log(pathname);
	//location.pathname !=='/' ? console.log('111') : console.log('222');
	
	//const onSetActiveFn = (to) => {
    //  console.log("active", to);
    //}

	const history = useHistory();
	
    const scrollTarget = (target) => scroller.scrollTo(target, {
		smooth: true, 				
		duration: 500,
		offset: -65,
		//spy: false,
		delay: 0
		//onSetActive: onSetActiveFn
		
	});

    const scrollToPage = async (target) => {
		if (location.pathname !=='/') {
			await history.push('/');
			scrollTarget(target);
		}
		Events.scrollEvent.remove("end");	
	};	
      
	return (
		<>    
			<Navbar expand="lg" className="nav-top fixed-top" >
				<Container>
					<Navbar.Brand>
						<Nav>
						  <Link activeClass="active" to="scrollLink1" spy={true} smooth={true} duration={500} offset={-150}>
								<span className="logo-container">
								<img className="logo-dark"  src={require('../assets/images/logo-dark.png')} alt="Web Molecules"/>
								<img className="logo-light"  src={require('../assets/images/logo-light.png')} alt="Web Molecules"/>
						  </span>
						  <span className="logo-text"><span>Web</span><span>Molecules</span></span>
						</Link>
						</Nav>
					</Navbar.Brand>
					<Navbar.Toggle aria-controls="basic-navbar-nav"/>              
					<Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
						<Nav>
							{/*
							{data.links && data.links.map(({id, name, to}) => 
								<Link 
									key={id} 
									activeClass="active" 
									className="nav-link"
									spy={true} 
									to={to}
									smooth={true} 
									duration={500} 
									offset={-65}  
									onClick={() => scrollToPage(to)}
								>
									{name}
								</Link>	
							)}
							*/}
								{data.links && data.links.map((item, index) => 
									<Link 
										key={item.id} 
										activeClass="active" 
										className="nav-link"
										spy={true} 
										to={item.to}
										smooth={true} 
										duration={500} 
										offset={-65}  
										onClick={() => scrollToPage(item.to)}
									>
										{item.name}
									</Link>	
								)}		
						</Nav>
					</Navbar.Collapse>
				</Container>
			</Navbar>          
		</>
	);    
 }
  
export default NavigationTop;