import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import data from "../data/contact.json";

export default class Ecommerce extends Component { 
  render() {    
    return (
      <div>
        <Container className="content">
          <Row>
            <Col> 
              <h1 className="intro__title">
                Ecommerce
              </h1>
              <p className="intro__subtitle">
                {data.contactNumber}  |  {data.emailId}  | Chat Now!
              </p>
            </Col>
          </Row>
          <Row>           
            <Col md={4} className="text-end">
                <p>Get a Quote</p>
                <p>Subscribe to Newsletter</p>
                <p>Let's connect on social media</p>
                <p>Blog</p>

                <p>FAQ</p>               
            </Col>
            <Col md={8}>
              <p>{data.intro}</p>
            </Col>     
          </Row>        
        </Container>
      </div>
     );
  }  
}