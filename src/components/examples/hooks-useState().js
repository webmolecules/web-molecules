import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';

export default class Footer extends Component { 
  render() {    
    return (
		<div className="footer">
			<Container className="content">
			    <Row>
					<Col>
						<p className="intro__subtitle">
					   Copyright &copy;2021 Web Molecules. All rights reserved. 
							<NavLink to="/page">Privacy Policy</NavLink> and <NavLink to="/page">Terms of use</NavLink>
						</p>			
					</Col>
			    </Row>
			</Container>
		</div>
     );
  }  
}