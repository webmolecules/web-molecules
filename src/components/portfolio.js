import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link, Element, Events, animateScroll, scrollSpy, scroller } from "react-scroll";
import { NavLink } from 'react-router-dom'; 
import ReactDOM from "react-dom";
import Slider from "react-slick";
import data from "../data/portfolio.json";
import submenuData from "../data/submenu.json";
import SectionHeader from './section-header';

import img1 from '../assets/images/stock-vector-website-template-design-modern-vector-illustration-concept-of-web-page-design-for-website-and-1714630837.jpg';
import img2 from '../assets/images/stock-vector-website-template-design-modern-vector-illustration-concept-of-web-page-design-for-website-and-1-171462.jpg';
import img3 from '../assets/images/stock-vector-website-template-design-modern-vector-illustration-concept-of-web-page-design-for-website-and-1714627627.jpg';
import img4 from '../assets/images/stock-vector-creative-website-template-design-vector-illustration-concept-of-web-page-design-for-website-and-105-1059847622.jpg';
import bgImg from '../assets/images/stock-photo-travel-woman-choosing-lanterns-in-hoi-an-vietnam-1477602647.jpg';

export default class Portfolio extends Component {
	
	constructor(props) {
		super(props);    
		this.slide = this.slide.bind(this);
	} 

	slide(y){
		y > 0 ? (
		   this.slider.slickNext()
		) : (
		   this.slider.slickPrev()
		)
	}  

	state = {
		scrollTo : "scrollLink1",
		title : "Our Creations",
		subTitle : "Explore our creations",
		navBg : bgImg,	
		portfolio : [
			{
			  "id": 1,
			  "name": "test1",
			  "img" : img1
			},
			{
			  "id": 2,
			  "name": "test2",
			  "img" : img2
			},
			{
			  "id": 3,
			  "name": "test3",
			  "img" : img3
			},
			{
			  "id": 4,
			  "name": "test4",
			  "img" : img4
			}
		  ]
	} 

	componentDidMount() {  
		//let slickListDiv = ReactDOM.findDOMNode(this.refs.slickSlider);
		//slickListDiv.addEventListener('wheel', this.handleSlickSliderScroll);
		let slickListDiv = document.getElementsByClassName('slick-list')[0];
		slickListDiv.addEventListener('wheel', event => {
			event.preventDefault()
			event.deltaY > 0 ? this.slider.slickNext() : this.slider.slickPrev()
		});
		// window.addEventListener('wheel', (e) => {
		//     this.slide(e.wheelDelta);
		// })
	}

	componentWillUnmount() {
		document.getElementsByClassName('slick-list')[0].removeEventListener('wheel' , event => {
		  event.preventDefault()
		  event.deltaY > 0 ? this.slider.slickNext() : this.slider.slickPrev()
		});
	}

	render() {  
		var bgImg1 = {		
			//backgroundImage: 'url(' + require(`${data.portfolioBg}`) + ')'
			backgroundImage: 'url(' + data.portfolioBg + ')',		
		}	

		const settings = {
			dots: true,
			infinite: true,
			//speed: 300,
			//centerMode: true,
			variableWidth: true,
			//className: "center",
			//centerPadding: "0px",
			//centerMode: true,
			arrows: false,
			slidesToShow: 1,
			//slidesToScroll: 1,
			//slidesPerRow: 2,      
			drggable: true,
			//rows: 2,
			//autoplay: true,
			speed: 800,			 
			//autoplaySpeed: 3000,
			cssEase: "linear"
			//pauseOnHover: false
		};
		
		return (			
			<div className="section-container">
				<SectionHeader 
					title={this.state.title} 
					subTitle={this.state.subTitle} 
					links={submenuData.portfolio}
					scrollTo={this.state.scrollTo}
					navBg={this.state.navBg}					
				/>   
				<Container fluid className="px-0">
					<Row className="g-0">        
						<Col xs={12} className="portfolio">						
							<Slider {...settings} ref={slider => this.slider = slider}>    
							{/*
								{data.portfolio && data.portfolio.map((item, index) => {
								  let imgSrc = this.state.title;
								  console.log(imgSrc);
								 // let src = require(imgSrc)
									return (
										<div key={index}>
										    <div  className="slide-content ratio ratio-4x3">
												<div>
												  <img src={require('../assets/images/bg-design.png')} />                         
												</div>
										    </div>
									    </div>   
									)}
								)}
							*/}
							{this.state.portfolio.map(({id, name, img}) => {
								  let imgSrc = img
								  //console.log(imgSrc);
								 // let src = require(imgSrc)
									return (
										<div key={id} className="slide-content ">
											<img  src={img} />
											<div className="detail">
												<div>
													<h4>Lorem ipsum dolor</h4>
													<p>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
													</p>
													<NavLink to="/page">read more</NavLink>
												</div>
											</div>
									    </div>   
									)}
								)}
							</Slider>
						</Col>
					</Row>
				</Container>				
			</div>
		);
	}  
}