import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Jumbotron from 'react-bootstrap/Jumbotron'
import Slider from "react-slick";

import img1 from '../assets/images/dummylogo-1.png';
import img2 from '../assets/images/dummylogo-2.png';
import img3 from '../assets/images/dummylogo-3.png';
import img4 from '../assets/images/dummylogo-4.png';
import img5 from '../assets/images/dummylogo-5.png';
import img6 from '../assets/images/dummylogo-6.png';

export default class Clients extends Component {
	constructor(props) {
		super(props);
		this.slide = this.slide.bind(this);
		this.state = {
			nav1: null,
			nav2: null,
			clients : [
						{
						  "id": 1,
						  "img" : img1,
						  "testimonial" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
						  "designation": "Director",
						  "name": "Devid Davies"
						},
						{
						  "id": 2,
						  "img" : img2,
						  "testimonial" : "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
						  "designation": "Media Director",
						  "name": "John Smith"
						},
						{
						  "id": 3,
						  "img" : img3,
						  "testimonial" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
						  "designation": "CEO",
						  "name": "Peter Brown"
						},
						{
						  "id": 4,
						  "img" : img4,
						  "testimonial" : "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
						  "designation": "Director",
						  "name": "Devid Smith"
						},
						{
						  "id": 5,
						  "img" : img5,
						  "testimonial" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
						  "designation": "Media Director",
						  "name": "Robert Wilson"
						},
						{
						  "id": 6,
						  "img" : img6,
						  "testimonial" : "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
						  "designation": "CEO",
						  "name": "James Taylor"
						}
					  ]
		};
	}
  
	slide(y){
		y > 0 ? (
		   this.slider1.slickNext()
		) : (
		   this.slider1.slickPrev()
		)
	}
	
	componentDidMount() {
		//let slickListDiv = ReactDOM.findDOMNode(this.refs.slickSlider);
		//slickListDiv.addEventListener('wheel', this.handleSlickSliderScroll);
		let slickListDiv = document.getElementsByClassName('slick-list')[2];
		slickListDiv.addEventListener('wheel', event => {
			event.preventDefault()
			event.deltaY > 0 ? this.slider1.slickNext() : this.slider1.slickPrev()
		});
		//window.addEventListener('wheel', (e) => {
			//this.slide(e.wheelDelta);
		//})
		this.setState({
			nav1: this.slider1,
			nav2: this.slider2
		});
	}
	
	componentWillUnmount() {
		document.getElementsByClassName('slick-list')[2].removeEventListener('wheel' , event => {
		  event.preventDefault()
		  event.deltaY > 0 ? this.slider.slickNext() : this.slider.slickPrev()
		});
	}

	render() {
	    const slider2settings = {
			className: "center",
			centerMode: true,
			centerPadding: '0px',
			arrows: false,
			dots: false,
			infinite: true,      
			slidesToShow: 3,
			swipe: false,
			//swipeToSlide: true,
			slidesToScroll: 1,
			focusOnSelect: true,			     
			drggable: false,			
			//autoplay: true,
			speed: 1000,
			//autoplaySpeed: 4000,
			pauseOnHover: false
		};
		
		const slider1settings = {
			fade: true,
			arrows: false,
			dots: false,
			infinite: true,      
			slidesToShow: 1,			
			slidesToScroll: 1,
			speed: 1000,
			pauseOnHover: false
		};
		
		//Background image
		var bgImg = {
		  backgroundImage: 'url(' + require('../assets/images/breaket-down.jpg') + ')', 
		}
		
		return (
			<>
				<Row className="gx-60 justify-content-center clients">	
					<Col xs="12">					 
						<h4 className="hading">Trusted by</h4>
						<Slider {...slider2settings} ref={slider => this.slider2 = slider} asNavFor={this.state.nav1}>   
						{this.state.clients.map(({id, img}) => {
							return (
								<div key={id}>
									<div  className="slide-content ratio ratio-2x1">
										<div>
										  <img src={img} />               
										  </div>
									</div>
								</div>   
							)}
						)}
						</Slider>	
						<div style={bgImg} className="clients-testimonial">
							<Slider {...slider1settings} asNavFor={this.state.nav2}
									  ref={slider => (this.slider1 = slider)}>
								{this.state.clients.map((item, index) => {
									return (
										<div key={index}>
											<div>
												<p>&quot; {item.testimonial} &quot;</p>
												<strong>{item.name}</strong>
												<p>{item.designation}</p>
											</div>
										</div>   
									)}
								)}
							</Slider>	
						</div>					
					</Col>
				</Row>
			</>
		);
	}
}