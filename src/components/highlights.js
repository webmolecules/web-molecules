import React, { Component } from 'react';
import { Col } from 'react-bootstrap';
import Slider from "react-slick";
import data from "../data/about.json";

export default class Highlights extends Component {
	render() {		
		const highlightSettings = {
			fade: true,
			arrows: false,
			dots: false,
			infinite: true,      
			slidesToShow: 2,
			//swipe: true,
			//swipeToSlide: true,
			slidesToScroll: 2,
			//focusOnSelect: true,			     
			drggable: true,			
			autoplay: true,
			speed: 1000,
			autoplaySpeed: 4000,
			pauseOnHover: false
		};

		return (
			<Col md={6} className="highlights">
				<Slider {...highlightSettings}>   
					{data.highlights && data.highlights.map(({ title, id, highlight, bgColor }) => (
						<div key={id} className="highlight">
							<div style={{backgroundColor: bgColor}}>
								<div>
									<h3 className="title">{title}</h3>
									<p>{highlight}</p>
								</div>
							</div>								
						</div>
					))}
				</Slider>
			</Col>
		);
	}  
}