"use strict";

import React, { Component } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch } from 'react-router-dom'; 

import ScrollToTop from './ScrollToTop';
import NavTop from './navigation-top';
import Footer from './footer';
import Index from './index';
import Home from './home';
import Page from './page';
import Practice from './practice/practice';
import FormTest from './practice/test-1/form';//test 1
import Dropdown from './practice/test-2/dropdown';//test 2
import QuizApp from './practice/test-3/quiz-app';//test 3
import ReactReduxCounter from './practice/test-4/react-redux-counter';//test 4
import Notfound from './notfound';

export default class App extends Component {
	//componentDidMount() {
		//let navEle = ReactDOM.findDOMNode(this.refs.navRef);
		//console.log(navEle.clientHeight);  
	//}
  
	render() {
		
		const components = [
			{ name: `Index`, path: `/` },
			{ name: `Home`, path: `/home` },			
			{ name: `Page`, path: `/page` },
			{ name: `Practice`, path: `/practice` },			
			{ name: `FormTest`, path: `/form` },	
			{ name: `Dropdown`, path: `/dropdown` },
			{ name: `QuizApp`, path: `/quiz-app` },
			{ name: `ReactReduxCounter`, path: `/react-redux-counter` },
			{ name: `Notfound`, path: `/*` }
		];
		
		const COMPONENT_MAP = {
			Index: Index,
			Home: Home,			
			Page: Page,			
			Practice: Practice,			
			FormTest: FormTest,	
			Dropdown: Dropdown,	
			QuizApp: QuizApp,
			ReactReduxCounter: ReactReduxCounter,
			Notfound: Notfound
		};
  
		return (
			<>
				<BrowserRouter>
					<NavTop />
					<ScrollToTop />
					<Switch>
						{components.map((item, index) =>
						item.path == `/` ? (
							<Route key={index} exact path={item.path} component={COMPONENT_MAP[item.name]}/>
						)
						: item.path == `/*` ? (
							<Route key={index} path={item.path} component={COMPONENT_MAP[item.name]}/>
						)
						: (
							<Route key={index} path={item.path} component={COMPONENT_MAP[item.name]} />
						)
						)}               
					</Switch>  
					<Footer />
				</BrowserRouter>
			</>
		);
	}
  
}