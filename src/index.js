import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';


//const root = ReactDOM.createRoot(document.getElementById('root'));

//Imported in style.scss file
//import 'bootstrap/dist/css/bootstrap.css'; 
import './input.css';

import './assets/styles/style.scss';


ReactDOM.render(

   
    <React.StrictMode>
      <App />
    </React.StrictMode>
   ,document.getElementById('root')


);